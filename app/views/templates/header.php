<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman <?= $data['judul']; ?></title>
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="<?= BASEURL; ?>/home">PHP MVC</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <div class="navbar-nav"> <a class="nav-item nav-link active" href="<?= BASEURL; ?>">Home</a>
                    <a class="nav-item nav-link" href="<?= BASEURL; ?>/blog">Blog</a>
                    <a class="nav-item nav-link active" href="<?= BASEURL; ?>/about">About</a>

                    <?php if (isset($_SESSION['login'])) : ?>
                        <button type="login" class="btn btn-danger" id="login-button"><a href="<?= BASEURL; ?>/login/logout" class="text-white text-decoration-none">Logout</a></button>
                    <?php else : ?>
                        <button type="logout" class="btn btn-danger" id="logout-button"><a href="<?= BASEURL; ?>/login" class="text-white text-decoration-none">Login</a></button>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </nav>