<style>
    .banner {
        min-height: 100vh;
        background-image: url('<?= BASEURL; ?>/img/wisata.jpg');
        background-size: cover;
        background-position: center;
    }
</style>
<div class="container-fluid banner w-100 h-100 p-3 mx-auto text-center d-flex justify-content-center align-items-center text-white">
    <div class="container text-center">
        <h4 class="display-6">Selamat Datang</h4>
        <h3 class="display-1">Hai, Videographer</h3>
    </div>
</div>
