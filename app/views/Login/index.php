<div class="container my-5">
    <div class="d-flex justify-content-center">
        <div class="col-xl-10 col-lg-12">
            <div class="card border-0 shadow-lg">
                <div class="card-bodyp-0">
                    <div class="row">
                        <div class="col-lg-6" style="background-image: url('<?= BASEURL; ?>/img/wisata.jpg'); background-size:cover; background-position: center;"></div>
                        <div class="col-lg-6 p-5">
                            <div class="m-5">
                                <div class="mb-4 text-center">
                                    <h5>Sign In</h5>
                                    <p class="text-secondary">Please Sign In to continue</p>
                                </div>
                                <form action="<?= BASEURL; ?>/login/auth" method="post">
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Username</label>
                                        <input type="text" class="form-control" placeholder="username" name="username">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Password</label>
                                        <input type="password" class="form-control" placeholder="password" name="password">
                                    </div>
                                    <div class="row mb-3">
                                        <div class="d-flex justify-content-center">
                                            <div class="form-check">
                                                <input type="checkbox" name="" id="" class="form-check-input">
                                                <label for="" class="form-check-label">Remember me</label>
                                            </div>
                                            <div class="col text-end">
                                                <a href="" class="text-decoration-none text-danger">Forgot Password</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button class="btn bg-danger text-light mb-3" style="width: 100%;">Sign In</button>
                                        <p class="text-secondary">New user? <a href="<?= BASEURL; ?>/register" class="text-decoration-none text-danger">Sign Up</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>