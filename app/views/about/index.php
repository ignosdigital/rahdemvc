<style>
    body {
        background-color: #f4f4f4;
        font-family: Arial, sans-serif;
    }

    .about-me-card {
        background-color: #ffffff;
        border-radius: 10px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
    }
</style>
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="about-me-card p-4">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="<?= BASEURL; ?>/img/DSC00210.JPG" class="" style="object-fit:contain;" height="300px" width="300px" alt="">
                        </div>
                        <div class="col-md-8">
                            <h2>About Me</h2>
                            <p>
                                Saya adalah pelajar di SMK N 1 DENPASAR, Bali
                            </p>
                            <p>Nama : I Gusti Ngurah Oka Suardika <br>
                                Date of Birth : July, 20 2006 <br>
                                Adresss : Jln. Blumbungan, Sibang Kaja <br>
                                Email : rahde@gmail.com <br>
                                Phone : 085739378852
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>