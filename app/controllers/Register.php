<?php

class Register extends Controller
{
    private $table = 'users';
    private $db;


    public function __construct()
    {
        $this->db = new Database;
    }
    public function index()
    {
        if (isset($_SESSION['register'])) {
            header('Location: ' . BASEURL . '/home');
        }
        $data['judul'] = "Register";
        $this->view('templates/header', $data);
        $this->view('register/index');
    }

    public function prosesRegister()
    {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $cpassword = $_POST['cpassword'];
        if ($password !== $cpassword) {
            echo 'password tidak match';
        }
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);
        $query = "INSERT INTO users (username, email, password)  VALUES (:username, :email, :password)";
        $this->db->query($query);
        $this->db->bind('username', $username);
        $this->db->bind('email', $email);
        $this->db->bind('password', $passwordHash);
        $this->db->execute();
        header('Location: ' . BASEURL . '/home');
    }

    public function logout()
    {
        session_destroy();
        header('Location: ' . BASEURL . '/login');
        exit;
    }
}
