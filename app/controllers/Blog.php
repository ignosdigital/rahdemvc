<?php

class Blog extends Controller
{
    public function __construct()
    {
        if (!isset($_SESSION['login'])) {
            header('location: ' . BASEURL . '/login');
        }
    }
    public function index()
    {
        $data["judul"] = "Blog";
        $data["blog"] = $this->model("Blog_model")->getAllBlog();
        $this->view("templates/header", $data);
        $this->view("blog/index", $data);
        $this->view("templates/footer");
    }

    public function detail($id)
    {
        $data["judul"] = "Detail Blog";
        $data["blog"] = $this->model("Blog_model")->getModelById($id);
        $this->view("templates/header", $data);
        $this->view("blog/detail", $data);
        $this->view("templates/footer");
    }
}
