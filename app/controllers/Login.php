<?php

class Login extends Controller
{
    public function __construct()
    {
        if (isset($_SESSION['login'])) {
            header('location: ' . BASEURL . '/home');
        }
    }
    public function index()
    {
        $data['judul'] = "Login";
        $this->view('templates/header', $data);
        $this->view('login/index');
    }

    public function auth()
    {
        if ($this->model('User_model')->login($_POST) > 0) {
            $_SESSION['login'] = true;
            header('location: ' . BASEURL . '/home');
        } else {
        }
    }

    public function logout()
    {
        session_destroy();
        header('Location: ' . BASEURL . '/login');
        exit;
    }
}
