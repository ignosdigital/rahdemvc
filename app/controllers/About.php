<?php
class About extends Controller
{
    public function __construct()
    {
        if (!isset($_SESSION['login'])) {
            header('location: ' . BASEURL . '/login');
        }
    }
    public function index()
    {
        $data['judul'] = 'About Me';
        $this->view('templates/header', $data);
        $this->view('about/index', $data);
        $this->view('templates/footer');
    }
}
